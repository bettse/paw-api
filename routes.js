const AWS = require('aws-sdk')
const lambda = new AWS.Lambda()
const Sequelize = require('sequelize')
const models = require('./models')
const Op = Sequelize.Op

const DECIMAL = 10
const oneDay = 24 * 60 * 60 * 1000

const routes = [{
  method: 'GET',
  path: '/units',
  handler: available,
  config: {
    description: 'Available',
    notes: 'Returns the available units and their prices for the current and previous day',
    tags: ['api', 'unit']
  }
}, {
  method: 'GET',
  path: '/units/{id}',
  handler: unit,
  config: {
    description: 'History',
    notes: 'Returns the details and rent history for a specified unit',
    tags: ['api', 'history', 'detail']
  }
}, {
  method: 'GET',
  path: '/breakdown/{type}',
  handler: breakdown,
  config: {
    description: 'Breakdown',
    notes: 'Returns various breakdowns of the units or their histories',
    tags: ['api', 'breakdown']
  }
}, {
  method: 'GET',
  path: '/login',
  handler: login,
  config: {
    auth: 'simple',
    description: 'Login',
    notes: 'Route that always requires authentication for development use',
    tags: ['api', 'debug']
  }
}, {
  method: 'POST',
  path: '/scrape',
  handler: scrape,
  config: {
    description: 'Scrape',
    notes: 'Outputs message to scrape lambda for one-off scrape',
    tags: ['api', 'scrape']
  }
}]

if (process.env.BASIC_AUTH_USERNAME && process.env.BASIC_AUTH_PASSWORD) {
  routes.forEach((route) => {
    route.config.auth = 'simple'
  })
}

function login (request, reply) {
  return reply({
    message: 'success'
  })
}

function breakdown (request, reply) {
  const { type } = request.params
  if (type === 'all') {
    breakdownAll(request, reply)
  } else if (type === 'beds') {
    breakdownBeds(request, reply)
  }
}

function breakdownBeds (request, reply) {
  models.Rent.findAll({
    include: [{
      model: models.Unit,
      attributes: []
    }],
    group: ['date', 'Unit.beds'],
    attributes: [
      'date',
      [Sequelize.col('Unit.beds'), 'beds'],
      [Sequelize.fn('COUNT', 'date'), 'count']
    ],
    order: [
      ['date', 'ASC']
    ]
  }).then((breakdown) => {
    // Not sure how to do this in SQL
    const grouped = breakdown.reduce((acc, rent) => {
      const { date, beds, count } = rent.get({plain: true})
      // This is basically rolling up sets of records (0 beds, 1 beds, 2 beds) into a single records
      const forDate = acc[date] || {date}
      forDate[beds] = parseInt(count, DECIMAL)
      acc[date] = forDate
      return acc
    }, {})
    //Object.values() isn't available in the node version on aws
    const values = Object.keys(grouped).map((key) => grouped[key])
    reply(values)
  })
}

function breakdownAll (request, reply) {
  models.Rent.findAll({
    group: ['date'],
    attributes: ['date', [Sequelize.fn('COUNT', 'date'), 'count']],
    order: [
      ['date', 'ASC']
    ]
  }).then(reply)
}

function available (request, reply) {
  models.Audit.findOne({
    attributes: { exclude: ['id', 'createdAt', 'updatedAt'] },
    order: [ ['createdAt', 'DESC'] ]
  }).then((lastRun) => {
    const dayBefore = new Date((new Date(lastRun.date)).getTime() - oneDay)
    models.Unit.findAll({
      order: [['number', 'ASC']],
      attributes: { exclude: ['createdAt', 'updatedAt'] },
      include: [{
        model: models.Rent,
        attributes: { exclude: ['id', 'UnitNumber', 'createdAt', 'updatedAt'] },
        where: { [Op.or]: [{ date: lastRun.date }, { date: dayBefore }] }
      }]
    }).then(units => {
      const response = { lastRun, units }
      reply(response)
    })
  })
}

function unit (request, reply) {
  const UnitNumber = parseInt(request.params.id, DECIMAL)
  models.Unit.findOne({
    where: {number: UnitNumber},
    attributes: { exclude: ['createdAt', 'updatedAt'] },
    include: [{
      model: models.Rent,
      attributes: { exclude: ['id', 'UnitNumber', 'createdAt', 'updatedAt'] }
    }],
    order: [
      [models.Rent, 'date', 'ASC']
    ]
  }).then(reply)
}

function scrape (request, reply) {
  const stackery_ports = JSON.parse(process.env.STACKERY_PORTS)
  const payload = { time: (new Date()).toISOString() }
  const outputIndex = 0
  const inputIndex = 0
  const params = {
    FunctionName: stackery_ports[outputIndex][inputIndex].functionName,
    Payload: JSON.stringify(payload)
  }

  return lambda.invoke(params, function (err, data) {
    if (err) console.log(err, err.stack)
    return reply({
      data,
      message: 'success'
    })
  })
}

module.exports = routes
