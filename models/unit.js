const Sequelize = require('sequelize')

module.exports = function (sequelize, DataTypes) {
  const Unit = sequelize.define('Unit', {
    number: {
      type: Sequelize.INTEGER,
      primaryKey: true
    },
    sqft: Sequelize.INTEGER,
    floorplan: Sequelize.STRING, // Letter indicating floorplan style
    beds: Sequelize.INTEGER,
    baths: Sequelize.INTEGER,
    views: Sequelize.STRING, // One or more letters for directions of windows (N/S/E/W)
    misc: { // Other info, like having a study/den
      type: Sequelize.STRING,
      defaultValue: ''
    }
  })

  Unit.associate = function (models) {
    Unit.hasMany(models.Rent)
  }

  return Unit
}
