require('dotenv').config()

const Hapi = require('hapi')
const Inert = require('inert')
const Vision = require('vision')
const Swaggered = require('hapi-swaggered')
const BasicAuth = require('hapi-auth-basic')
const Routes = require('./routes')

const config = {}
const server = new Hapi.Server(config)

const port = process.env.PORT || 8888
const host = process.env.HOST || '0.0.0.0'
server.connection({
  port,
  host,
  routes: { cors: true }
})

const swaggeredRegister = {
  register: Swaggered,
	options: {
    cors: true,
    info: {
    	title: 'PAW API',
      description: 'Powered by node, hapi, hapi-swaggered, and sequelize',
      version: '1.0'
    }
  }
}

const validate = function (request, username, password, callback) {
  const { BASIC_AUTH_USERNAME, BASIC_AUTH_PASSWORD } = process.env

  if (BASIC_AUTH_USERNAME && BASIC_AUTH_PASSWORD) {
    if (username === BASIC_AUTH_USERNAME && password === BASIC_AUTH_PASSWORD) {
      return callback(null, true, {username})
    }

    return callback(null, false)
  }

  // Valid by default
  return callback(null, true, {})
}


// Were we included from another file?
const local = !module.parent

server.register([Vision, Inert, swaggeredRegister, BasicAuth], function (err) {
  if (err) {
    console.error('Failed loading plugins')
    process.exit(1)
  }

  server.auth.strategy('simple', 'basic', { validateFunc: validate });
  server.route(Routes)

  // Only start listening for requests if running server locally
  if (local) {
    server.start(function () {
      console.log('Server running at:', server.info.uri)
    })
  }
})

module.exports = server
